package com.example.logsParser.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigInteger;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name="logs")
@NoArgsConstructor
public class Log {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "date")
    private String date;

    @Column(name = "player_id")
    private Long playerId;

    @Column(name = "reward_id")
    private Long rewardId;

    @Column(name = "hash")
    private String hash;

    @Column(name = "mark")
    private Long mark;
}
