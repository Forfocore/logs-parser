package com.example.logsParser.service;

import com.example.logsParser.entity.Log;
import com.example.logsParser.repository.LogRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@RequiredArgsConstructor
public class LogService {

    private final LogRepository logRepository;

    public void logs() {
        ExecutorService service = Executors.newFixedThreadPool(4);
        service.execute(new Runnable() {
            @Override
            public void run() {
                File file = new File("game1.txt");
                try (FileReader fr = new FileReader(file);
                     BufferedReader br = new BufferedReader(fr)) {
                    String line = br.readLine();
                    int index;
                    String current;
                    while (line != null) {
                        line.replaceAll("\u0000", "");
                        List<String> words = new ArrayList<>(Arrays.asList(line.split(" ")));
                        Log log = new Log();
                        if ( (words.contains("StepAdvanced,") && words.contains("response")) || (words.contains("RegisterStep,") && words.contains("request")) ) {
                            log.setDate(words.get(0));
                            index = words.indexOf("mark") + 1;
                            current = words.get(index).substring(2, words.get(index).length() - 1);
                            log.setMark(Long.valueOf(current));
                            index = words.indexOf("playerId") + 2;
                            current = words.get(index).substring(0, words.get(index).length() - 1);
                            log.setPlayerId(Long.valueOf(current));
                            List<String> lastWord = new ArrayList<>(Arrays.asList(words.get(words.size()-1).split("\"")));
                            index = lastWord.indexOf("rewardId");
                            if (index != -1) {
                                log.setRewardId(Long.valueOf(lastWord.get(index + 1).substring(1, lastWord.get(index + 1).length()-1)));
                            }
                            index = lastWord.indexOf("playerStepHash");
                            if (index != -1) {
                                log.setHash(lastWord.get(index + 2));
                                logRepository.save(log);
                            }
                        }
                        line = br.readLine();
                    }
                }
                catch (Exception e) {
                    throw new RuntimeException(e);
                }
            }
        });

    }

    public List<Log> getAll() {
        List<Log> logs = logRepository.findAll();
        return logs;
    }

}
