package com.example.logsParser.controller;

import com.example.logsParser.entity.Log;
import com.example.logsParser.service.LogService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/logs")
public class LogController {

    private final LogService logService;

    @PostMapping("/create")
    public void createLogs() {
        logService.logs();
    }

    @GetMapping("/all")
    public List<Log> getAll() {
        List<Log> logs = logService.getAll();
        return logs;
    }
}
